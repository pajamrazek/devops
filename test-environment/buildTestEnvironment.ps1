#Prerequisites: Installed doctl and kubectl, generated token

# login with token to digital ocean
doctl auth init

# recreate Kubernetes image registry
$registry = "devops-image-registry"
doctl registry delete -f $registry
doctl registry create $registry

# push an image to the registry and run it
$app = "robot-dreams-training-app"
$appPort = 8090

doctl registry login

# recreate the Kubernetes cluster
$cluster = "devops-k8s-cluster"
doctl kubernetes cluster delete -f $cluster
doctl kubernetes cluster create $cluster --tag do-devops --auto-upgrade=true --node-pool "name=mypool;count=2;auto-scale=true;min-nodes=1;max-nodes=3;tag=do-devops"

# authorize the cluster to use the registry so it can download images
doctl registry kubernetes-manifest | kubectl apply -f -
kubectl patch serviceaccount default -p '{\"imagePullSecrets\": [{\"name\": \"registry-devops-image-registry\"}]}'


# Deploy the app behind a load balancer
#kubectl create deployment $app --image=registry.digitalocean.com/$($registry)/$($app)
#kubectl expose deployment $app --type=LoadBalancer --port=80 --target-port=$appPort
